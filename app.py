import sqlite3
from flask import Flask, request, render_template, jsonify, make_response
from flask.ext.sqlalchemy import SQLAlchemy
import datetime, calendar

app = Flask(__name__)
app.debug = True
if app.debug:
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///debugdb/temperatures.db'
else:
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////var/db/temperatures.db'

db = SQLAlchemy(app)

class Temperature(db.Model):
    __tablename__ = 'temperatures'
    id = db.Column(db.Integer, primary_key=True)
    sensorid = db.Column(db.Integer)
    temperature = db.Column(db.Float(precision=3))
    timestamp = db.Column(db.DateTime, default=datetime.datetime.now)


# Helping function to add months to timedelta
def add_months(sourcedate, months):
    month = sourcedate.month - 1 + months
    year = sourcedate.year + month / 12
    month = month % 12 + 1
    day = min(sourcedate.day,calendar.monthrange(year,month)[1])
    return datetime.date(year,month,day)


def convertToTimeframe(temperatures, minutes=False, hours=True, days=True, months=True):
    """ Takes temperatures query """
    """ Returns list of objects {'timestamp': datetime-object, 'temperature': int} """

    ids = 0
    returnlist = []
    for t in temperatures:
        entry = {}

        minute = 0
        if minutes:
            minute = t.timestamp.minute

        hour = 0
        if hours:
            hour = t.timestamp.hour

        day = 1
        if days:
            day = t.timestamp.day

        month = 1
        if months:
            month = t.timestamp.month

        entry['timestamp'] = datetime.datetime(t.timestamp.year, month, day, hour, minute)
        for e in returnlist:
            if e['timestamp'] == entry['timestamp']:
                e['temperature'] = (e['temperature'] + t.temperature) / 2
                break
        else:
            entry['temperature'] = t.temperature
            returnlist.append(entry)

    return sorted(returnlist, key=lambda entry: entry['timestamp'])


def getTemperatureList(temperatures):
    returnlist = []
    count = 0
    for t in temperatures:
        count += 1
        obj = {
            'id': count,
            'temperature': '{0:.1f}'.format(t.temperature),
            'time': str(t.timestamp.strftime('%H:%M')),
            'date': t.timestamp.strftime('%d.%m.%Y')
        }
        if t.temperature < 8:
            obj['class'] = 'danger'
        elif t.temperature < 12:
            obj['class'] = 'warning'
        returnlist.append(obj)
    return returnlist


def getChartData(temperatures, label):

    chart = {
        'labels' : [],
        'datasets' : [
            {
                'fillColor' : "rgba(151,187,205,0)",
                'strokeColor' : "#e67e22",
                'pointColor' : "rgba(255,255,255,1.0)",
                'pointStrokeColor' : "#e67e22",
                'data' : []
            }
        ]
    }

    min_temperature = 999
    max_temperature = 0
    for t in temperatures:
        chart['labels'].append(str(t['timestamp'].strftime(label)))
        chart['datasets'][0]['data'].append('{0:.1f}'.format(t['temperature']))
        if t['temperature'] > max_temperature:
            max_temperature = t['temperature']
        if t['temperature'] < min_temperature:
            min_temperature = t['temperature']

    options = {
        'bezierCurve' : False,
        'scaleOverride' : True,
        'scaleSteps' : (max_temperature + 6 - min_temperature),
        'scaleStepWidth' : 1,
        'scaleStartValue' : min_temperature - 3,
    }

    return {'chart': chart, 'options': options}


@app.route('/')
def angularjs():
    return render_template('app.html')

@app.route('/ng/<template>')
def returnAngularTemplate(template):
    return make_response(open('ngtemplates/' + template).read())

@app.route('/json')
def json():
    temperatures = Temperature.query.all()
    return jsonify(entries = getTemperatureList(temperatures))

@app.route('/json/stats')
def json_stats():

    stats = {}
    temperatures = Temperature.query.all()

    max_temp = 0
    min_temp = 999

    for t in temperatures:
        if t.temperature > max_temp:
            max_temp = t.temperature
            max_temp_timestamp = t.timestamp
        if t.temperature < min_temp:
            min_temp = t.temperature
            min_temp_timestamp = t.timestamp
    stats['max_temp'] = {
        'value': '{0:.1f}'.format(max_temp),
        'timestamp': max_temp_timestamp.strftime('%d.%m.%Y %H:%M')
    }
    stats['min_temp'] = {
        'value': '{0:.1f}'.format(min_temp),
        'timestamp': min_temp_timestamp.strftime('%d.%m.%Y %H:%M')
    }
    stats['row_count'] = len(temperatures)

    return jsonify(stats)

@app.route('/json/latest')
def json_latest():
    t = Temperature.query.order_by('-id').first()

    if t >= 22:
        color = "180,25,15"
    elif t >= 19:
        color = "210,110,10"
    elif t >= 16:
        color = "50,190,90"
    elif t >= 12:
        color = "50,190,180"
    elif t >= 5:
        color = "0,100,180"
    else:
        color = "50,70,255"

    temperature = {
        'timestamp': t.timestamp.strftime('%d.%m.%Y kello %H:%M'),
        'temperature': '{0:.1f}'.format(t.temperature),
        'color': color
    }
    return jsonify(temperature)

@app.route('/json/chart')
def json_chart():

    q_day = 1
    q_month = 0
    q_year = 0
    q_timeframe = ''

    if request.args.get('month'):
        q_month = int(request.args.get('month')) + 1 # month is 0-11
    if request.args.get('day'):
        q_day = int(request.args.get('day'))
    if request.args.get('year'):
        q_year = int(request.args.get('year'))
    if request.args.get('timeframe'):
        q_timeframe = request.args.get('timeframe')

    querydate = datetime.date(q_year, q_month, q_day)

    if q_timeframe == 'day':
        temperatures = Temperature.query.filter(
            Temperature.timestamp < querydate + datetime.timedelta(1),
            Temperature.timestamp > querydate
        )
        data = convertToTimeframe(temperatures, hours=True)
        chart = getChartData(data, '%H')
    elif q_timeframe == 'month':
        startdate = datetime.date(querydate.year, querydate.month, 1)
        enddate = add_months(datetime.date(querydate.year, querydate.month, 1), 1)
        temperatures = Temperature.query.filter(
            Temperature.timestamp < enddate,
            Temperature.timestamp > startdate
        )
        data = convertToTimeframe(temperatures, hours=False, days=True)
        # if only 1 day:
        if len(data) == 1:
            data.append(data[0])
        chart = getChartData(data, '%d')
    elif q_timeframe == 'year':
        temperatures = Temperature.query.all()
        data = convertToTimeframe(temperatures, hours=False, days=False, months=True)
        # if only 1 month:
        if len(data) == 1:
            data.append(data[0])
        chart = getChartData(data, '%y/%m')
    elif q_timeframe == 'alltime':
        temperatures = Temperature.query.all()
        data = convertToTimeframe(temperatures, hours=False, days=False, months=False)
        # if only 1 year:
        if len(data) == 1:
            data.append(data[0])
        chart = getChartData(data, '%Y')

    return jsonify(chart)

# For local development
if __name__ == '__main__':
    app.run()
