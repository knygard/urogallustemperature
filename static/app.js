var app = angular.module('app', ['ngResource', 'ngRoute', '$strap.directives']);

var settings = {
}

/* CONFIG */

app.value('$strapConfig', {
  datepicker: {
    language: 'fi',
    format: 'MM d, yyyy',
  }
});

app.config(function($routeProvider) {

  $routeProvider
    .when('/', {
        templateUrl: 'ng/front.html',
        controller: 'FrontPageController'
    })
    .when('/chart', {
        templateUrl: 'ng/chart.html',
        controller: 'ChartController'
    })
    .when('/stats', {
        templateUrl: 'ng/stats.html',
        controller: 'StatsController'
    })
    .when('/alldata', {
        templateUrl: 'ng/alldata.html',
        controller: 'AllDataController'
    })
    .otherwise({redirectTo : '/'});

});

app.factory('TemperatureFactory', function($resource) {
  return $resource('/json', {},
    { 'get': {method: 'GET', isArray: false} });
});

app.factory('CurrentTemperatureFactory', function($resource) {
  return $resource('/json/latest', {},
    { 'get': {method: 'GET', isArray: false} });
});

app.factory('StatsFactory', function($resource) {
  return $resource('/json/stats', {},
    { 'get': {method: 'GET', isArray: false} });
});

app.factory('ChartFactory', function($resource) {
    var today = new Date();
    return $resource('/json/chart', {
            'year': today.getFullYear(),
            'month': today.getMonth(),
            'day': today.getDate(),
            'timeframe': 'day'  // default timeframe
        },
        { 'get': {method: 'GET', isArray: false} });
    $scope.test = $strapConfig;
});

/* CONTROLLERS */

app.controller('NavController', function($scope, $location) {
    $scope.isActive = function (viewLocation) {
        return viewLocation === $location.path();
    };
});

app.controller('FrontPageController', function($scope, CurrentTemperatureFactory) {
    $scope.temperaturedata = CurrentTemperatureFactory.get();
});

app.controller('AllDataController', function($scope, TemperatureFactory) {
    $scope.alldata = TemperatureFactory.get();
    $scope.predicate = '-id';
});

app.controller('StatsController', function($scope, StatsFactory) {
    $scope.stats = StatsFactory.get();
});

app.controller('ChartController', function($scope, ChartFactory) {
    $scope.dayChartData = ChartFactory.get();
    $scope.monthChartData = ChartFactory.get({'timeframe': 'month'});
    $scope.yearChartData = ChartFactory.get({'timeframe': 'year'});

    var today = new Date();
    $scope.datepicker = {
      "date": today
    }

    $scope.$watch("datepicker.date", function(newDate, oldDate) {
        if (newDate != oldDate) {
            var dayChartData = ChartFactory.get({
                    'year': newDate.getFullYear(),
                    'month': newDate.getMonth(),
                    'day': newDate.getDate()
            }, function() {
                $scope.dayChartData = dayChartData;
            });

            var monthChartData = ChartFactory.get({
                    'year': newDate.getFullYear(),
                    'month': newDate.getMonth(),
                    'day': newDate.getDate(),
                    'timeframe': 'month'
            }, function() {
                $scope.monthChartData = monthChartData;
            });
        }
    }, true);
});

app.controller('TimeFrameController', function($scope, ChartFactory) {

    $scope.setDay = function() {
        $scope.chartDate = ChartFactory.get({
            'year': 2013,
            'month': 11
        });
    }

});
/* DIRECTIVES */

app.directive("chartjs", function() {
    return {
        restrict: "A",
        scope: {
                data: "=",
                type: "@",
                options: "=",
                id: "@"
        },
        link: function($scope, $elem) {
            var ctx = $elem[0].getContext("2d");
            var chart = new Chart(ctx);

            $scope.$watch("data", function (newVal, oldVal) {
                if (newVal != oldVal && newVal) {
                    chart[$scope.type]($scope.data, $scope.options);
                }
            }, true);
        }
    }
});


