# UrogallusTemperature

This is a temperature monitoring system I built for our summer cottage. One can see the temperature now or from the database in a chart view.

It uses my [TemperatureLogger](https://bitbucket.org/knygard/temperaturelogger) to keep the database up to date.

## Technology

Backend: Flask

Frontend: AngularJS, Bootstrap, ChartJS

## Screenshots

##### Temperature now

![Temperature now](http://bytebucket.org/knygard/urogallustemperature/raw/c58d901b87205da45f5d02ca0b5e9b6a45f9a0d1/screenshots/temperature_now.png)

##### Charts

![Temperature now](http://bytebucket.org/knygard/urogallustemperature/raw/c58d901b87205da45f5d02ca0b5e9b6a45f9a0d1/screenshots/charts.png)

##### Stats

![Temperature now](http://bytebucket.org/knygard/urogallustemperature/raw/c58d901b87205da45f5d02ca0b5e9b6a45f9a0d1/screenshots/stats.png)