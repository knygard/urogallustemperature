from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
import datetime

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////var/db/temperatures.db'
db = SQLAlchemy(app)

class Temperature(db.Model):
    __tablename__ = 'temperatures'
    id = db.Column(db.Integer, primary_key=True)
    sensorid = db.Column(db.Integer)
    temperature = db.Column(db.Float(precision=3))
    timestamp = db.Column(db.DateTime, default=datetime.datetime.now)

temperatures = Temperature.query.all()
for t in temperatures:
    print t.id + t.temperature
